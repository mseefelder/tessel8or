# Development notes:

## N-vertex patches tesselation:

For N-vertex patches tesselation, the  index buffer (wich is and GL_ELEMENT_ARRAY) must be formated  as an **array of n-vertex patches** \! (sounds obvious, but isn't).

So, if one wants to do tesselation with 16 vertex patches, the array will have groups of 16 vertices.


## [Approximating Catmull-Clark Subdivision Surfaces with Bicubic Patches](http://research.microsoft.com/en-us/um/people/cloop/accTOG.pdf)

#### Notes on implementation:

Steps for implementation lacking *tangent patches*:
* Take a quad mesh;
* For each quad face, discover neighbourhood and store it in a *16-vertex patch structure*;
* This *16-patch* buffer will be used for rendering.

The above method looks sufficient to render quad meshes with 4-valence for all vertices.

Further reading of the paper will clarify how to generalize technique.