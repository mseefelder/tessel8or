# Useful references:

## Tesselation:
* [Tesselation: OpenGL wiki page](https://www.opengl.org/wiki/Tessellation);
* [Presentation about Tesselation](http://web.engr.oregonstate.edu/~mjb/cs519/Handouts/tessellation.1pp.pdf)
* [The Little Grasshopper: Triangle Tessellation with OpenGL 4.0](http://prideout.net/blog/?p=48);
* [The Little Grasshopper: Quad Tessellation with OpenGL 4.0](http://prideout.net/blog/?p=49);
	* [Notes on patches (bicubic)](http://mrl.nyu.edu/~perlin/courses/spring2009/splines4.html)

## Curves rasterization:
* [Rasterization of Parametric Curves using Tessellation Shaders in GLSL](https://computeranimations.wordpress.com/2015/03/16/rasterization-of-parametric-curves-using-tessellation-shaders-in-glsl/);

## Subdivision surfaces:
* [Catmull-Clark Surfaces](http://www.idav.ucdavis.edu/education/CAGDNotes/Catmull-Clark.pdf);
* [Approximating Catmull-Clark Subdivision Surfaces with Bicubic Patches](http://research.microsoft.com/en-us/um/people/cloop/accTOG.pdf);
* [Real-Time Creased Approximate Subdivision Surfaces](http://denkovacs.com/media/publications/pdf/RealTimeCreasedACC.pdf);
* [GPU Smoothing of Quad Meshes](http://www.cise.ufl.edu/research/SurfLab/papers/smi08.pdf);
* [Fast Parallel Construction of Smooth Surfaces from Meshes with Tri/Quad/Pent Facets](http://www.cise.ufl.edu/research/SurfLab/papers/08poly.pdf);

## Extra
* [10 Fun things to do with tesselation shaders](http://www.ludicon.com/castano/blog/2009/01/10-fun-things-to-do-with-tessellation/);