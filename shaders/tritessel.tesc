#version 430

layout(vertices = 3) out;
in vec3 vPosition[];
out vec3 tcPosition[];
uniform float inner;
uniform float outer;


#define ID gl_InvocationID

void main()
{
    tcPosition[ID] = vPosition[ID];
    if (ID == 0) {
        gl_TessLevelInner[0] = inner;//TessLevelInner;
        gl_TessLevelOuter[0] = outer;//TessLevelOuter;
        gl_TessLevelOuter[1] = outer;//TessLevelOuter;
        gl_TessLevelOuter[2] = outer;//TessLevelOuter;
    }
}
