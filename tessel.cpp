#include "tessel.hpp"

Tessel::Tessel(void)
{
}

Tessel::~Tessel()
{}


void Tessel::initialize (int width, int height)
{
    // initialize the shader effect
    tesselator.setShadersDir("../shaders/");
    tesselator.initialize();

	flycamera.setPerspectiveMatrix(60.0, width/(float)height, 0.1f, 100.0f);
	// left side of the window (window x dim = 2*width+20)
	flycamera.setViewport(Eigen::Vector2f ((float)width, (float)height));

	tri.createParallelepiped(1.0,1.0,1.0);

	MeshImporter::loadObjFile(&quad, "../models/4quadCube.obj");
	quad.normalizeModelMatrix();

	renderquad = false;
	
}

void Tessel::paintGL (void)
{
   	flycamera.updateViewMatrix();
   	Eigen::Vector4f viewport = flycamera.getViewport(); 
	
 	glClearColor(0.9, 0.9, 0.9, 0.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	
	if(renderquad)
		tesselator.render(quad, flycamera, light);
	else
		tesselator.render(tri, flycamera, light);
	// render coordinate system at lower right corner
	//flycamera.renderAtCorner();
}

void Tessel::changeMesh(void)
{
	renderquad = tesselator.swapShader();
}