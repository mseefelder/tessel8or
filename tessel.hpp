#ifndef __TESSEL__
#define __TESSEL__

#include <GL/glew.h>

#include <tesselator.hpp>
//#include <utils/flycamera.hpp>
//#include <utils/path.hpp>
#include <shapes/camerarep.hpp>
#include <mesh.hpp>
#include <utils/objimporter.hpp>
#include <utils/plyimporter.hpp>
#include <utils/trackball.hpp>
#include "trackballcamera.hpp"

using namespace std;

class Tessel 
{

public:

    explicit Tessel(void);
    ~Tessel();
    
    /**
     * @brief Initializes the shader effect
	 * @param width Window width in pixels
	 * @param height Window height in pixels
     */
    void initialize(int width, int height);

    /**
     * Repaints screen buffer.
     **/
    virtual void paintGL();

	/**
	* Returns the pointer to the flycamera instance
	* @return pointer to flycamera
	**/
	//Flycamera* getCamera(void)
	Tucano::Trackballcamera* getCamera(void)
	{
		return &flycamera;
	}

	Effects::Tesselator* getEffect(void)
	{
		return &tesselator;
	}

	void changeMesh(void);

private:

	// A simple phong shader for rendering meshes
    Effects::Tesselator tesselator;

	// A fly through camera
	//Tucano::Flycamera flycamera;
	Tucano::Trackballcamera flycamera;

	// Light represented as a camera
	Tucano::Camera light;
	
	// Meshes
	Mesh tri;
	Mesh quad;

	//Which mesh should be rendered
	bool renderquad;
};

#endif // TESSEL
