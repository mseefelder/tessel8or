#ifndef __TESSELATOR__
#define __TESSELATOR__

#include <tucano.hpp>
#include <camera.hpp>

using namespace Tucano;

namespace Effects
{

/**
 * @brief Renders a mesh using a Tesselator shader.
 */
class Tesselator : public Effect
{

private:

    /// Tesselator Shader
    Shader tesselator_shader;

    /// Default color
    Eigen::Vector4f default_color;

    /// Number of vertices per patch, inner subdiv, outer subdiv
    int patchVertices;
    float inner, outer;

    /// Shader swap state
    bool swapState;

public:

    /**
     * @brief Default constructor.
     */
    Tesselator (void)
    {
        default_color << 0.7, 0.4, 0.1, 1.0;
        patchVertices = 3;
        inner = 1.0;
        outer = 1.0;
        swapState = false;
    }

    /**
     * @brief Default destructor
     */
    virtual ~Tesselator (void) {}

    /**
     * @brief Load and initialize shaders
     */
    virtual void initialize (void)
    {
        // searches in default shader directory (/shaders) for shader files tesselatorShader.(vert,frag,geom,comp,tesc,tese)
        loadShader(tesselator_shader, "tritessel");
    }

    bool swapShader (void)
    {
        if (swapState)
        {
            loadShader(tesselator_shader, "tritessel");
            patchVertices = 3;
        }
        else
        {
            loadShader(tesselator_shader, "quadtessel");
            patchVertices = 4;
        }

        swapState = !swapState;
        return swapState;
    }

    void innerSum ( float x )
    {
        inner += x;
    }

    void outerSum ( float x )
    {
        outer += x;
    }

    /**
    * @brief Sets the default color, usually used for meshes without color attribute
    */
    void setDefaultColor ( Eigen::Vector4f& color )
    {
        default_color = color;
    }

    /** * @brief Render the mesh given a camera and light, using a Tesselator shader 
     * @param mesh Given mesh
     * @param camera Given camera 
     * @param lightTrackball Given light camera 
     */
    void render (Tucano::Mesh& mesh, const Tucano::Camera& camera, const Tucano::Camera& lightTrackball)
    {

        Eigen::Vector4f viewport = camera.getViewport();
        glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

        tesselator_shader.bind();

        // sets all uniform variables for the tesselator shader
        tesselator_shader.setUniform("projectionMatrix", camera.getProjectionMatrix());
        tesselator_shader.setUniform("modelMatrix", mesh.getModelMatrix());
        tesselator_shader.setUniform("viewMatrix", camera.getViewMatrix());
        tesselator_shader.setUniform("lightViewMatrix", lightTrackball.getViewMatrix());
        tesselator_shader.setUniform("default_color", default_color);
        tesselator_shader.setUniform("inner", inner);
        tesselator_shader.setUniform("outer", outer);

        mesh.setAttributeLocation(tesselator_shader);

        glEnable(GL_DEPTH_TEST);

        // Custom "mesh.render();"  --
        mesh.bindBuffers();
        glPatchParameteri(GL_PATCH_VERTICES, patchVertices);
        glDrawElements(GL_PATCHES, mesh.getNumberOfElements(),GL_UNSIGNED_INT, 0);
        mesh.unbindBuffers();
        //                          --

        tesselator_shader.unbind();
    }


};

}


#endif
